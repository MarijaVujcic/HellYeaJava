package net.gym.springboot.demo.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import net.gym.springboot.demo.model.Employee;
import net.gym.springboot.demo.model.Role;
import net.gym.springboot.demo.repository.EmployeeRepository;
import net.gym.springboot.demo.repository.RoleRepository;
import net.gym.springboot.demo.web.dto.UserRegistrationDto;

@Service
public class EmployeeServiceImpl implements EmployeeService {


    
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private RoleRepository roleRepository;

    
   
    public Employee findByEmail(String email)
    {
        return employeeRepository.findByEmail(email);
    }

     
    public Employee save(UserRegistrationDto registration, Integer which){
        Employee user = new Employee();
        user.setFirstName(registration.getFirstName());
        user.setLastName(registration.getLastName());
        user.setEmail(registration.getEmail());
        user.setPassword(passwordEncoder.encode(registration.getPassword()));
        user.setRoles(Arrays.asList(roleRepository.findById( which).get()));
        return employeeRepository.save(user);
    }
   

    //overidane funkcije za onaj UserService
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Employee employee = employeeRepository.findByEmail(email);
        if ( employee == null){
            throw new UsernameNotFoundException("Nope dont exist, or wrong creditentials.");
        }
      
            return new org.springframework.security.core.userdetails.User(employee.getEmail(),
            employee.getPassword(),
            mapRolesToAuthorities(employee.getRoles()));

        
       
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public void saveEmpl(Employee e, Integer i) {
        Employee user = new Employee();
        user.setFirstName(e.getFirstName());
        user.setLastName(e.getLastName());
        user.setEmail(e.getEmail());
        user.setPassword(passwordEncoder.encode(e.getPassword()));
        user.setRoles(Arrays.asList(roleRepository.findById(i).get()));
        employeeRepository.save(user);
    }

}
