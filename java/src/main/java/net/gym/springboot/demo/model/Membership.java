package net.gym.springboot.demo.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Membership
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Boolean active;

    private Date dateOfMemb; //samo se izgenerira kad pravim membership
 
    //korisnik
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User member;

    @OneToMany(mappedBy = "selected")
    private Set<Selected_days>  selected_days;
    //placanje- određena cijena koja se plaća mjesečno,godišnji
    @ManyToOne
    private Program programs;

    @ManyToOne
    private WayOfPay wayOfPay;

    @ManyToOne
    private Paying pays;
    
    


    // getters, setters, other fields
    @OneToMany(mappedBy = "member")
    private Set<Month>  years;
    
    public Membership(){}
    public Membership(User m, Program p, Paying pay)
    {
        Date date = new Date(System.currentTimeMillis());
        this.active=true;
        this.dateOfMemb=  date;
        this.member=m;
        this.pays=pay;
        this.programs=p;

      }
    /**
     * @return Member return the member
     */
    public User getMember() {
        return member;
    }

    /**
     * @param member the member to set
     */
    public void setMember(User member) {
        this.member = member;
    }


    /**
     * @return Date return the dateOfMemb
     */
    public Date getDateOfMemb() {
        return dateOfMemb;
    }

    /**
     * @param dateOfMemb the dateOfMemb to set
     */
    public void setDateOfMemb(Date dateOfMemb) {
        this.dateOfMemb = dateOfMemb;
    }


    /**
     * @return boolean return the active
     */
    public Boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(Boolean active) {
        this.active = active;
    }


    


    /**
     * @return Paying return the pays
     */
    public Paying getPays() {
        return pays;
    }

    /**
     * @param pays the pays to set
     */
    public void setPays(Paying pays) {
        this.pays = pays;
    }


    /**
     * @return Program return the programs
     */
    public Program getPrograms() {
        return programs;
    }

    /**
     * @param programs the programs to set
     */
    public void setPrograms(Program programs) {
        this.programs = programs;
    }


    


    /**
     * @return WayOfPay return the wayOfPay
     */
    public WayOfPay getWayOfPay() {
        return wayOfPay;
    }

    /**
     * @param wayOfPay the wayOfPay to set
     */
    public void setWayOfPay(WayOfPay wayOfPay) {
        this.wayOfPay = wayOfPay;
    }

    
  




    /**
     * @return Integer return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }




    /**
     * @return Set<Selected_days> return the selected_days
     */
    public Set<Selected_days> getSelected_days() {
        return selected_days;
    }

    /**
     * @param selected_days the selected_days to set
     */
    public void setSelected_days(Set<Selected_days> selected_days) {
        this.selected_days = selected_days;
    }


    /**
     * @return Set<Month> return the years
     */
    public Set<Month> getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(Set<Month> years) {
        this.years = years;
    }

}