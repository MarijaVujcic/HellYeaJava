package net.gym.springboot.demo.web.dto;


public class ConfigurationDto
{
    private Integer userU;
    private Integer coach;
    private Integer prog;
    private Integer pay;
    private Integer roleD;
    private Integer wayP;
	private Integer month;



    /**
     * @return Integer return the prog
     */
    public Integer getProg() {
        return prog;
    }

    /**
     * @param prog the prog to set
     */
    public void setProg(Integer prog) {
        this.prog = prog;
    }
	/**
     * @return Integer return the prog
     */
    public Integer getMonth() {
        return month;
    }

    /**
     * @param prog the prog to set
     */
    public void setMonth(Integer prog) {
        this.month = prog;
    }


    /**
     * @return Integer return the pay
     */
    public Integer getPay() {
        return pay;
    }

    /**
     * @param pay the pay to set
     */
    public void setPay(Integer pay) {
        this.pay = pay;
    }


    /**
     * @return Integer return the wayP
     */
    public Integer getWayP() {
        return wayP;
    }

    /**
     * @param wayP the wayP to set
     */
    public void setWayP(Integer wayP) {
        this.wayP = wayP;
    }


    /**
     * @return Integer return the userU
     */
    public Integer getUserU() {
        return userU;
    }

    /**
     * @param userU the userU to set
     */
    public void setUserU(Integer userU) {
        this.userU = userU;
    }

    /**
     * @return Integer return the coach
     */
    public Integer getCoach() {
        return coach;
    }

    /**
     * @param coach the coach to set
     */
    public void setCoach(Integer coach) {
        this.coach = coach;
    }


    /**
     * @return Integer return the roleD
     */
    public Integer getRoleD() {
        return roleD;
    }

    /**
     * @param roleD the roleD to set
     */
    public void setRoleD(Integer roleD) {
        this.roleD = roleD;
    }

}