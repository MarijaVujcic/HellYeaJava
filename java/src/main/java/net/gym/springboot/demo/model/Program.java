package net.gym.springboot.demo.model;



import java.util.Set;

import javax.persistence.*;
// vrsta programa , vjezbe i to
@Entity
public class Program
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer program_id;

    //kao kardio, weights, kondicijska, mršavljenje
    private String typeOfWorkout;
    
    //trener ima više programa jedan program ima samo jednog trebera
    
    
    @OneToMany(mappedBy = "programs")
    private Set<Membership>  memberships;
 
    private String description;
    
    

    

    /**
     * @return String return the typeOfWorkout
     */
    public String getTypeOfWorkout() {
        return typeOfWorkout;
    }

    /**
     * @param typeOfWorkout the typeOfWorkout to set
     */
    public void setTypeOfWorkout(String typeOfWorkout) {
        this.typeOfWorkout = typeOfWorkout;
    }

    
   


    /**
     * @return String return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }


    


    /**
     * @return Integer return the program_id
     */
    public Integer getProgram_id() {
        return program_id;
    }

    /**
     * @param program_id the program_id to set
     */
    public void setProgram_id(Integer program_id) {
        this.program_id = program_id;
    }

    /**
     * @return Set<Membership> return the memberships
     */
    public Set<Membership> getMemberships() {
        return memberships;
    }

    /**
     * @param memberships the memberships to set
     */
    public void setMemberships(Set<Membership> memberships) {
        this.memberships = memberships;
    }

}