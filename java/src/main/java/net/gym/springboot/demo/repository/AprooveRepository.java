package net.gym.springboot.demo.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import net.gym.springboot.demo.model.AprooveDay;

public interface AprooveRepository extends JpaRepository<AprooveDay, Integer>
{
    // @Query("select * from AprooveDay a where a.usersID = ?1 and a.aprooved = true ")
    // List<AprooveDay> findAproveddays(Integer u);
}