package net.gym.springboot.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.gym.springboot.demo.model.Selected_days;

public interface SelectedRepository extends JpaRepository<Selected_days, Integer>
{
    
}