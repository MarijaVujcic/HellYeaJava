package net.gym.springboot.demo.web;

import java.sql.Date;
import java.text.DateFormatSymbols;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import net.gym.springboot.demo.model.AprooveDay;
import net.gym.springboot.demo.model.ChoosenDay;
import net.gym.springboot.demo.model.Membership;
import net.gym.springboot.demo.model.Selected_days;
import net.gym.springboot.demo.model.User;
import net.gym.springboot.demo.repository.*;
import net.gym.springboot.demo.service.UserService;
import net.gym.springboot.demo.web.dto.InformationsDTO;
import net.gym.springboot.demo.web.dto.WrapperChooseingDay;

//jos moram stavit odabir dana, statistika
@Controller
public class UserController {

    private String message = "";

    @Autowired
    UserService userService;

    @Autowired
    MonthRepository monthRepository;

    @Autowired
    MembershipRepository membershipRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PayingRepository payingRepository;

    @Autowired
    WayOfPayRepository wayPayingRepository;

    @Autowired
    ProgramRepository programRepository;

    @Autowired
    ChoosenRepository choosenRepository;
    @Autowired
    AprooveRepository aprove;
    @Autowired
    SelectedRepository selectedRepo;

    @GetMapping("/welcome")
    public ModelAndView welcomeCheck(Model model) {
        // Day todaDay = new Day();
        // todaDay.setDay(Date.valueOf(LocalDate.now()));
        // this.dayRepository.save(todaDay);
        ModelAndView m = new ModelAndView();
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        User current = userService.findByEmail(loggedInUser.getName());
        InformationsDTO info = new InformationsDTO();

        if (current.getMembership() == null) {
            m.addObject("informations", info);

            m.addObject("programs", this.programRepository.findAll());
            m.addObject("name", current.getFirstName());
            m.addObject("message", this.message);
            m.addObject("payings", this.payingRepository.findAll());
            m.addObject("wayofpay", wayPayingRepository.findAll());
            m.setViewName("formular");
        } else {
            if(!current.getMembership().isActive())
            {
                m.addObject("name", current.getFirstName());
                m.addObject("nameAnd", "Welcome back ".concat(current.getFirstName()).concat("! Your account is active again :)"));
                m.setViewName("welcome");
                this.membershipRepository.getOne(current.getMembership().getId()).setActive(true);
                this.membershipRepository.save(this.membershipRepository.getOne(current.getMembership().getId()));
            }
            else{
            m.addObject("name", current.getFirstName());
            m.addObject("nameAnd", "");
            m.setViewName("welcome");

            }
            
            
        }
        this.message = "";
        return m;
    }

    @PostMapping("/welcome")
    public ModelAndView checkingForm(@Valid @ModelAttribute("informations") InformationsDTO info) {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        User current = userService.findByEmail(loggedInUser.getName());
        Membership newM = new Membership();
        ModelAndView modelAndView = new ModelAndView("redirect:/welcome");

        if (info.getSelectedPay() != null && info.getWayPaying() != null) {
            newM.setPays(payingRepository.findById(info.getSelectedPay()).get());

        } else {
            this.message = "You must choose paying!";

            modelAndView.setViewName("redirect:/welcome");
            return modelAndView;
        }
        if (info.getWayPaying() != null && info.getWayPaying() == 2) {
            if (info.getIban() != null && info.getIban().length() == 21) {
                current.setIban(info.getIban());
                newM.setWayOfPay(wayPayingRepository.getOne(info.getWayPaying()));
            } else {
                this.message = "You must write iban, you choosen card!";

                modelAndView.setViewName("redirect:/welcome");
                return modelAndView;

            }
        }
        newM.setWayOfPay(wayPayingRepository.getOne(info.getWayPaying()));
        if (info.getSelectedProgram() != null) {
            newM.setPrograms(this.programRepository.findById(info.getSelectedProgram()).get());
        }

        current.setHeight(info.getHeight());
        current.setWeight(info.getWeight());
        newM.setActive(true);
        newM.setMember(current); 
        newM.setDateOfMemb(Date.valueOf( LocalDate.now()));
         this.membershipRepository.saveAndFlush(newM);
        current.setMembership(newM);
        
      
        this.userRepository.save(current);
        this.message = "";
        return modelAndView;
    }


    @GetMapping(value = "/welcome/settings")
    public ModelAndView showSettings() {
        if (choosenRepository.findAll().size() == 0) {
            String[] namesOfDays = DateFormatSymbols.getInstance().getShortWeekdays();
            for (int i = 1; i < 7; i++) {
                ChoosenDay d = new ChoosenDay(namesOfDays[i]);
                choosenRepository.saveAndFlush(d);
            }
        }
        ModelAndView model = new ModelAndView();
        InformationsDTO change = new InformationsDTO();
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        User current = userService.findByEmail(loggedInUser.getName());
        Membership p = this.membershipRepository.findById(current.getMembership().getId()).get();

        change.setHeight(current.getHeight());
        change.setWeight(current.getWeight());
        model.addObject("payings", this.payingRepository.findAll());
        model.addObject("programs", this.programRepository.findAll());
        model.addObject("selectedPay", p.getPays().getName());
        model.addObject("selectedProgram", p.getPrograms().getTypeOfWorkout());
        model.addObject("name", current.getFirstName());
        model.addObject("informations", change);
        model.addObject("lista", new WrapperChooseingDay());
        model.addObject("li", current.getMembership().getPays().getTimeAWeek());
        model.addObject("days", choosenRepository.findAll());
        model.addObject("mess", this.message);
        model.addObject("wayofpay", wayPayingRepository.findAll());
        model.setViewName("settings");
        return model;

    }

    @PostMapping(value = "/welcome/settings")
    public ModelAndView saveSettings(@Valid @ModelAttribute("informations") InformationsDTO info) {

        this.message="";
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        User current = userService.findByEmail(loggedInUser.getName());
        ModelAndView m = new ModelAndView();
        Membership p = this.membershipRepository.findById(current.getMembership().getId()).get();
        if (info.getSelectedProgram() == null) {
            info.setSelectedProgram(p.getPrograms().getProgram_id());
        }
        else{
            p.setPrograms(this.programRepository.getOne(info.getSelectedProgram()));
        }
        if (info.getSelectedPay() == null) {
            info.setSelectedPay(p.getPays().getId());
        }
       
        if (!info.getSelectedPay().equals(p.getPays().getId())) {
            String date = LocalDate.now().toString();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate end = LocalDate.parse(date, formatter);
            LocalDate now = LocalDate.parse(date, formatter);
            LocalDate first = LocalDate.parse(date, formatter);
            first.withDayOfMonth(1);
            end = end.withDayOfMonth(end.getMonth().length(end.isLeapYear()));
            if (end == now || first == now) {
                p.setPays(this.payingRepository.getOne(info.getSelectedPay()));
            }

        }
        if (info.getWayPaying() != null && info.getWayPaying() == 2) {
            if (info.getIban() != null && info.getIban().length() == 21) {
                current.setIban(info.getIban());
                p.setWayOfPay(wayPayingRepository.getOne(info.getWayPaying()));
            } else {
                this.message = "You must write iban, you choosen card!";

                m.setViewName("redirect:/welcome/settings");
                return m;

            }
        }
        if (info.getDaz() != null && info.getDaz() != Date.valueOf(LocalDate.of(Calendar.getInstance().get(Calendar.YEAR),1,1))) {

            AprooveDay ap = new AprooveDay();
            ap.setDay(info.getDaz());
            ap.setUsersID(current.getId());
            aprove.saveAndFlush(ap);

        }
        current.setHeight(info.getHeight());
        current.setWeight(info.getWeight());
        this.userRepository.saveAndFlush(current);
        this.membershipRepository.save(p);
        this.membershipRepository.flush();
        this.message="";
        m.setViewName("redirect:/welcome/settings");
        return m;
    }

    @GetMapping(value = "/welcome/deactivate")
    public ModelAndView deactivateAccount(Model model) {
         Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
         User current = userService.findByEmail(loggedInUser.getName());
        ModelAndView m = new ModelAndView();
        if(current == null)
        {
            m.setViewName("redirect:/");
        }
        this.membershipRepository.findById(current.getMembership().getId()).get().setActive(false);
        this.membershipRepository.saveAndFlush(this.membershipRepository.findById(current.getMembership().getId()).get());
        m.setViewName("redirect:/logout");
        return m;
      
    }

    @GetMapping(value = "/welcome/statistic")
    public ModelAndView showStatistic() {
        ModelAndView m = new ModelAndView();
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        User current = userService.findByEmail(loggedInUser.getName());
        m.setViewName("statistic");
        m.addObject("name", current.getFirstName().concat(" ").concat(current.getLastName()));
        m.addObject("beendates", this.monthRepository.findMemb(current.getMembership().getId()));
        return m;
    }

    @PostMapping(value = "/welcome/addDays")
    public ModelAndView saveDays(@Valid @ModelAttribute("lista") WrapperChooseingDay lista) {
        this.message="";
        ModelAndView m = new ModelAndView();
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        User current = userService.findByEmail(loggedInUser.getName());
        if (lista == null) {
            m.setViewName("redirect:/welcome");
            return m;
        }
        List<Integer> tmp = new ArrayList<Integer>();
        for(int i=0;i<lista.getListIds().size();i++)
        {
            if(!tmp.isEmpty())
            {
                 if(tmp.contains(lista.getListIds().get(i).getIds()))
                 {
                     this.message="You must choose different days!";
                     m.setViewName("redirect:/welcome/settings");
                     return m;
                 }
              
            }

            tmp.add(lista.getListIds().get(i).getIds());
        
            
        }
        if (this.membershipRepository.getOne(current.getMembership().getId()).getSelected_days().isEmpty()) {

            for (int i = 0; i < lista.getListIds().size(); i++) {
                Selected_days newSelected = new Selected_days();
                newSelected.setSelected(this.membershipRepository.getOne(current.getMembership().getId()));
                newSelected.setChosen(this.choosenRepository.getOne(lista.getListIds().get(i).getIds()));
                this.selectedRepo.saveAndFlush(newSelected);

            }
        } else {
            this.selectedRepo
                    .deleteAll((this.membershipRepository.getOne(current.getMembership().getId()).getSelected_days()));

            for (int i = 0; i < lista.getListIds().size(); i++) {
                Selected_days newSelected = new Selected_days();
                newSelected.setSelected(this.membershipRepository.getOne(current.getMembership().getId()));
                newSelected.setChosen(this.choosenRepository.getOne(lista.getListIds().get(i).getIds()));
                this.selectedRepo.saveAndFlush(newSelected);

            }
        }

        m.setViewName("redirect:/welcome/settings");
        return m;
    }

}