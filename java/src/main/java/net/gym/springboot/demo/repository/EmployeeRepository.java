package net.gym.springboot.demo.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import net.gym.springboot.demo.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	Employee findByEmail(String email);
	@Query("select e from Employee e join e.roles r where r.id = 3 ")
	List<Employee> selectCoach();




}
