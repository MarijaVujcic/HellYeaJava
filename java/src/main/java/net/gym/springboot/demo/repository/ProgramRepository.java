package net.gym.springboot.demo.repository;

import net.gym.springboot.demo.model.Program;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProgramRepository extends JpaRepository<Program, Integer>
{

	
}