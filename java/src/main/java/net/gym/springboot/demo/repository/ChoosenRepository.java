package net.gym.springboot.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.gym.springboot.demo.model.ChoosenDay;

public interface ChoosenRepository extends JpaRepository<ChoosenDay, Integer>
{
    
}