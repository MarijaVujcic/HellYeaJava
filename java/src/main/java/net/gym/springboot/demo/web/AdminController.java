package net.gym.springboot.demo.web;

import java.util.Calendar;
import java.util.List;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import net.gym.springboot.demo.model.AprooveDay;
import net.gym.springboot.demo.model.DayInMonth;
import net.gym.springboot.demo.model.Employee;
import net.gym.springboot.demo.model.Membership;
import net.gym.springboot.demo.model.Month;
import net.gym.springboot.demo.model.User;
import net.gym.springboot.demo.repository.AprooveRepository;
import net.gym.springboot.demo.repository.DaysRepo;
import net.gym.springboot.demo.repository.EmployeeRepository;
import net.gym.springboot.demo.repository.MembershipRepository;
import net.gym.springboot.demo.repository.MonthRepository;
import net.gym.springboot.demo.repository.PayingRepository;
import net.gym.springboot.demo.repository.ProgramRepository;
import net.gym.springboot.demo.repository.RoleRepository;
import net.gym.springboot.demo.repository.UserRepository;
import net.gym.springboot.demo.repository.WayOfPayRepository;
import net.gym.springboot.demo.service.EmployeeService;
import net.gym.springboot.demo.service.UserService;
import net.gym.springboot.demo.web.dto.ConfigurationAdd;
import net.gym.springboot.demo.web.dto.ConfigurationDto;
import net.gym.springboot.demo.web.dto.InformationsDTO;

//konfiguracija, dodavanje programa, novih admina, trenera, način plaćanja, praćenja plaćanja!
//dodat da je user bio odredeni dan u teretani/odobrit, 
@Controller
@RequestMapping("/admin")
public class AdminController {
    private String email;
    private String message = "";
    @Autowired
    EmployeeService employeService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserService userService;
    @Autowired
    UserRepository userRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    MembershipRepository membershipRepository;

    @Autowired
    ProgramRepository programRepository;

    @Autowired
    PayingRepository payingRepository;
    @Autowired
    WayOfPayRepository wpr;
    @Autowired
    MonthRepository month;

    @Autowired
    DaysRepo daysRepo;
    @Autowired
    AprooveRepository aproveRepo;

    @GetMapping
    public ModelAndView showConfig(Model model) {
        // if (dayRepository.findAll() == null || dayRepository.findAll().size() == 0) {
        //     Calendar cal = Calendar.getInstance();
        //     cal.set(Calendar.DAY_OF_YEAR, 1);
        //     Calendar c = Calendar.getInstance();
        //     Date start = cal.getTime();
        //     c.set(Calendar.DAY_OF_YEAR, 365);
        //     Date end =  c.getTime();
        //     Calendar calendar = new GregorianCalendar();
        //     calendar.setTime(start);
        //     while (calendar.getTime().before(end)) {
        //         Date result = calendar.getTime();
        //         Day day = new Day();
        //         day.setDayD((java.sql.Date) result);
        //         dayRepository.save(day);
        //         calendar.add(Calendar.DATE, 1);

        //     }

        // }
        ModelAndView m = new ModelAndView();
        m.setViewName("welcome");
        m.addObject("config", new ConfigurationDto());
		m.addObject("configPay", new ConfigurationDto());
        m.addObject("users", userRepository.findAll());
        m.addObject("employees", employeeRepository.findAll());
        m.addObject("programs", programRepository.findAll());
        m.addObject("payings", payingRepository.findAll());
        m.addObject("addconfig", new ConfigurationAdd());
        m.addObject("roles", roleRepository.findAll());
        m.addObject("coachs", employeeRepository.selectCoach());
        m.addObject("ways", wpr.findAll());
        m.addObject("aprove", new AprooveDay());
        m.addObject("aprovs", aproveRepo.findAll());
        m.addObject("message", this.message);
        return m;
    }

    @PostMapping("/config/delete")
    public ModelAndView deleteMember(@Valid @ModelAttribute("config") ConfigurationDto config) {
        ModelAndView m = new ModelAndView();

        if (config.getRoleD() != null && roleRepository.getOne(config.getRoleD()) != null && config.getRoleD() != 1) {
            roleRepository.delete(roleRepository.getOne(config.getRoleD()));

        }
        if (config.getUserU() != null && userRepository.getOne(config.getUserU()) != null) {

            if (userRepository.getOne(config.getUserU()).getMembership() != null) {
                membershipRepository.delete(userRepository.getOne(config.getUserU()).getMembership());
            }
            userRepository.delete(userRepository.getOne(config.getUserU()));

        }

        if (config.getCoach() != null && employeeRepository.getOne(config.getCoach()) != null) {
            Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
            Employee current = employeService.findByEmail(loggedInUser.getName());
            if (config.getCoach() == current.getId()) {
                m.setViewName("redirect:/admin");
            } else {

                if (employeeRepository.getOne(config.getCoach()).getPrograms() != null) {

                    programRepository.deleteAll(employeeRepository.getOne(config.getCoach()).getPrograms());
                }
                employeeRepository.delete(employeeRepository.getOne(config.getCoach()));
            }

        }
        if (config.getProg() != null && programRepository.getOne(config.getProg()) != null) {
            programRepository.delete(programRepository.getOne(config.getProg()));

        }

        if (config.getPay() != null && payingRepository.getOne(config.getPay()) != null) {
            payingRepository.delete(payingRepository.getOne(config.getPay()));
        }
        if (config.getWayP() != null) {
            wpr.delete(wpr.getOne(config.getWayP()));
        }

        m.setViewName("redirect:/admin");
        return m;
    }

    @PostMapping("/config/add")
    public ModelAndView add(@Valid @ModelAttribute("addconfig") ConfigurationAdd config) {
        this.message = "";
        ModelAndView m = new ModelAndView();
        if (config != null) {
            if (config.getMember() != null) {
                if (userRepository.findByEmail(config.getMember().getEmail()) == null
                        && config.getMember().getEmail() != "") {
                    userService.saveU(config.getMember());
                } else {
                    this.message = "User email already exist or you didn't put valid informations!";
                }

            }
            if (config.getEmployee() != null && config.getEmployee().getFirstName() != null
                    && config.getEmployee().getFirstName() != "") {
                if (employeService.findByEmail(config.getEmployee().getEmail()) == null) {
                    employeService.saveEmpl(config.getEmployee(), config.getRole());
                } else {
                    this.message = " Employee with that email already exist!";
                }
            }

            if (config.getPayingg() != null && !config.getPayingg().getName().isEmpty()) {
                payingRepository.saveAndFlush(config.getPayingg());
            }
            if (config.getProgramm() != null && !config.getProgramm().getTypeOfWorkout().isEmpty()) {
                programRepository.saveAndFlush(config.getProgramm());
            }
            if (config.getRola() != null && config.getRola().getName() != "") {
                roleRepository.saveAndFlush(config.getRola());
            }
            if (config.getWayPay().getWay() != null && config.getWayPay().getWay() != "") {
                wpr.save(config.getWayPay());
            }
        }
        m.setViewName("redirect:/admin");
        return m;
    }

    @GetMapping("/config/user")
    public ModelAndView showUsersConfig(@Valid @ModelAttribute("config") ConfigurationDto config) {
        ModelAndView m = new ModelAndView();
        m.setViewName("redirect:/admin");
        if (config != null) {
            if (config.getUserU() != null) {
                InformationsDTO info = new InformationsDTO();
                m.addObject("informations", info);

                if (userRepository.getOne(config.getUserU()).getMembership() != null) {
                    info.setSelectedPay(userRepository.getOne(config.getUserU()).getMembership().getPays().getId());
                    info.setSelectedProgram(
                            userRepository.getOne(config.getUserU()).getMembership().getPrograms().getProgram_id());

                }
                m.addObject("wayofpay", wpr.findAll());
                m.addObject("payings", payingRepository.findAll());
                m.addObject("programs", programRepository.findAll());
                m.addObject("email", userRepository.getOne(config.getUserU()).getEmail());
                this.email = userRepository.getOne(config.getUserU()).getEmail();
                m.setViewName("formular");

            }
        }
        return m;

    }

    @PostMapping("/config/user")
    public ModelAndView saveUsersConfig(@Valid @ModelAttribute("informations") InformationsDTO info) {
        ModelAndView m = new ModelAndView();
        if (userService.findByEmail(this.email) != null) {
            User current = userService.findByEmail(this.email);
            current.setHeight(info.getHeight());
            current.setWeight(info.getWeight());
            Membership newM = new Membership();
            if (info.getIban() != null && info.getIban().length() == 21) {
                current.setIban(info.getIban());
            } else {
                m.setViewName("formular");
                m.addObject("message", "Iban incorrect!");
            }

            if (this.payingRepository.findById(info.getSelectedPay()).get() != null) {
                newM.setPays(this.payingRepository.findById(info.getSelectedPay()).get());
            }
            if (this.programRepository.findById(info.getSelectedProgram()).get() != null) {
                newM.setPrograms(this.programRepository.findById(info.getSelectedProgram()).get());
            }

            current.setMembership(newM);
            this.membershipRepository.saveAndFlush(newM);
            this.userRepository.save(current);
        }

        m.setViewName("redirect:/admin");
        return m;
    }

    @PostMapping("/config/deactivate")
    public ModelAndView deactivateUser(@Valid @ModelAttribute("config") ConfigurationDto config) {
        ModelAndView m = new ModelAndView();
        if (userRepository.getOne(config.getUserU()) != null) {
            membershipRepository.getOne(userRepository.getOne(config.getUserU()).getMembership().getId())
                    .setActive(false);
            ;
        }
        m.setViewName("redirect:/admin");
        return m;
    }

    @PostMapping("/config/coachProgram")
    public ModelAndView configCoach(@Valid @ModelAttribute("config") ConfigurationDto config) {
        ModelAndView m = new ModelAndView();
        if (config != null) {
            if (config.getCoach() != null && config.getProg() != null) {
                employeeRepository.getOne(config.getCoach()).setProgram(programRepository.getOne(config.getProg()));
                employeeRepository.save(employeeRepository.getOne(config.getCoach()));
            }
        }
        m.setViewName("redirect:/admin");
        return m;
    }

    @PostMapping("/config/aprove")
    public ModelAndView aproveDay(@Valid @ModelAttribute("aprove") AprooveDay an) {
        ModelAndView m = new ModelAndView();

        AprooveDay a = aproveRepo.getOne(an.getId());
        List<Month> monthscurrent = this.month.findMemb(this.userRepository.getOne(a.getUsersID()).getMembership().getId());
        if ( monthscurrent.size() == 0)
        {
            
            Integer month = a.getDay().toLocalDate().getMonthValue();
            Month newMonth = new Month();
            DayInMonth dim = new DayInMonth();
            dim.setBeen(true);
            dim.setDayD(a.getDay());
            newMonth.setMember(this.userRepository.getOne(a.getUsersID()).getMembership());
            newMonth.setMonth(month);
            newMonth.setYear(Calendar.getInstance().get(Calendar.YEAR));
            this.month.saveAndFlush(newMonth);
            dim.setMonthS(newMonth);
             this.daysRepo.saveAndFlush(dim);
            
            
        }
        else
        {
            int count=0;
            Integer month = a.getDay().toLocalDate().getMonthValue();
            for(int i = 0 ;i < monthscurrent.size();i++)
            {
                if(monthscurrent.get(i).getMonth() == month )
                {
                    Month past = this.month.getOne(monthscurrent.get(i).getId());
                    DayInMonth dim = new DayInMonth();
                    dim.setBeen(true);
                    dim.setDayD(a.getDay());
                    past.getDays().add(dim);
                    this.daysRepo.save(dim);
                    this.month.save(past);
                    count ++;
                    break;

                }
            }
            if(count == 0)
            {
                Integer month2 = a.getDay().toLocalDate().getMonthValue();
                Month newMonth = new Month();
                DayInMonth dim = new DayInMonth();
                dim.setBeen(true);
                dim.setDayD(a.getDay());
                newMonth.setMember(this.userRepository.getOne(a.getUsersID()).getMembership());
                newMonth.setMonth(month2);
                newMonth.setYear(Calendar.getInstance().get(Calendar.YEAR));
                newMonth.getDays().add(dim);
                this.daysRepo.save(dim);
                this.month.save(newMonth);
            }
        }
        this.aproveRepo.delete(this.aproveRepo.getOne(an.getId()));

        m.setViewName("redirect:/admin");
        return m;
    }
	 @PostMapping("/config/payed")
    public ModelAndView addPayedUser(@Valid @ModelAttribute("configPay") ConfigurationDto config)
	{
		 ModelAndView m = new ModelAndView();
        m.setViewName("redirect:/admin");
        if (config != null) {
            if (config.getUserU() != null && config.getMonth() != null) {
					userRepository.getOne(config.getUserU()).getMembership(); //uvatit nekako misec i stavit paid plus ispisat u statistiku, probat sql napravit da se upisiva !
                    if(this.month.findMonth(config.getMonth(), this.userRepository.getOne(config.getUserU()).getMembership().getId())==null)
                    {
                        Month newMonth = new Month();
                        newMonth.setMember(this.userRepository.getOne(config.getUserU()).getMembership());
                        newMonth.setPayed(true);
                        newMonth.setYear(Calendar.getInstance().get(Calendar.YEAR));
                        newMonth.setMonth(config.getMonth());
                        this.month.saveAndFlush(newMonth);

                    }
                    else{
                        Month past=this.month.findMonth(config.getMonth(), this.userRepository.getOne(config.getUserU()).getMembership().getId());
                        past.setPayed(true);
                        this.month.saveAndFlush(past);
                    }
                    
            }
        }
			
		m.setViewName("redirect:/admin");
		return m;
	}
	
}