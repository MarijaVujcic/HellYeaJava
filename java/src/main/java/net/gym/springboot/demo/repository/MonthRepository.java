package net.gym.springboot.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import net.gym.springboot.demo.model.Month;

public interface MonthRepository extends JpaRepository<Month, Integer>
{
    @Query("select d from Month d where d.member.id = ?1")
    List<Month> findMemb(Integer memb);
    @Query("select d from Month d where d.month = ?1 and d.member.id= ?2")
    Month findMonth(Integer mon, Integer memb);
    
}