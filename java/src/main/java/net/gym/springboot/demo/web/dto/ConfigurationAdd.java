package net.gym.springboot.demo.web.dto;

import net.gym.springboot.demo.model.Employee;
import net.gym.springboot.demo.model.Paying;
import net.gym.springboot.demo.model.Program;
import net.gym.springboot.demo.model.Role;
import net.gym.springboot.demo.model.User;
import net.gym.springboot.demo.model.WayOfPay;

public class ConfigurationAdd {
    private Program programm;
    private Paying payingg;
    private User member;
    private Employee employee;
    private Integer role;
    private Role rola;
    private WayOfPay wayPay;

    /**
     * @return Program return the programm
     */
    public Program getProgramm() {
        return programm;
    }

    /**
     * @param programm the programm to set
     */
    public void setProgramm(Program programm) {
        this.programm = programm;
    }

    /**
     * @return Paying return the payingg
     */
    public Paying getPayingg() {
        return payingg;
    }

    /**
     * @param payingg the payingg to set
     */
    public void setPayingg(Paying payingg) {
        this.payingg = payingg;
    }

    /**
     * @return User return the member
     */
    public User getMember() {
        return member;
    }

    /**
     * @param member the member to set
     */
    public void setMember(User member) {
        this.member = member;
    }

    /**
     * @return Employee return the employee
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     * @param employee the employee to set
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     * @return Integer return the role
     */
    public Integer getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Integer role) {
        this.role = role;
    }

    /**
     * @return Role return the rola
     */
    public Role getRola() {
        return rola;
    }

    /**
     * @param rola the rola to set
     */
    public void setRola(Role rola) {
        this.rola = rola;
    }

    /**
     * @return WayOfPay return the wayPay
     */
    public WayOfPay getWayPay() {
        return wayPay;
    }

    /**
     * @param wayPay the wayPay to set
     */
    public void setWayPay(WayOfPay wayPay) {
        this.wayPay = wayPay;
    }

}