package net.gym.springboot.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import net.gym.springboot.demo.model.DayInMonth;

public interface DaysRepo extends JpaRepository<DayInMonth, Integer>
{
   
   
}