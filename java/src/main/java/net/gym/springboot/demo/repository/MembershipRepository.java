package net.gym.springboot.demo.repository;

import net.gym.springboot.demo.model.Membership;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MembershipRepository extends JpaRepository<Membership, Integer>
{

    @Query(value= "select m from Membership m where m.member.id = ?1",  nativeQuery = true)
	Membership findUsersMembbyId(Integer memb);
}