package net.gym.springboot.demo.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import net.gym.springboot.demo.model.Employee;

import net.gym.springboot.demo.web.dto.UserRegistrationDto;

@Service
public interface EmployeeService extends UserDetailsService {

    Employee findByEmail(String email);

    Employee save(UserRegistrationDto registration, Integer which);

    void saveEmpl(Employee e, Integer i);
}
