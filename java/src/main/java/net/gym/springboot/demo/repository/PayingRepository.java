package net.gym.springboot.demo.repository;




import net.gym.springboot.demo.model.Paying;
import org.springframework.stereotype.Repository;




import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface PayingRepository extends JpaRepository<Paying, Integer>
{
    
}