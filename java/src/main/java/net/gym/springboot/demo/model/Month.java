package net.gym.springboot.demo.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Month
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Integer month;
    private Integer year;
    @ManyToOne
    private Membership member;

    @OneToMany(mappedBy = "monthS")
    private Set<DayInMonth> days;
    
    private Boolean payed;
    


    /**
     * @return Integer return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return Integer return the month
     */
    public Integer getMonth() {
        return month;
    }

    /**
     * @param month the month to set
     */
    public void setMonth(Integer month) {
        this.month = month;
    }

    /**
     * @return Integer return the year
     */
    public Integer getYear() {
        return year;
    }

    /**
     * @param year the year to set
     */
    public void setYear(Integer year) {
        this.year = year;
    }

    /**
     * @return Membership return the member
     */
    public Membership getMember() {
        return member;
    }

    /**
     * @param member the member to set
     */
    public void setMember(Membership member) {
        this.member = member;
    }

    /**
     * @return Set<DayInMonth> return the days
     */
    public Set<DayInMonth> getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(Set<DayInMonth> days) {
        this.days = days;
    }

    /**
     * @return Boolean return the payed
     */
    public Boolean isPayed() {
        return payed;
    }

    /**
     * @param payed the payed to set
     */
    public void setPayed(Boolean payed) {
        this.payed = payed;
    }
    
    /**
     * @param payed the payed to set
     */
    public void setOneDay(DayInMonth da) {
        this.days.add(da);
    }

}