package net.gym.springboot.demo.model;

import java.util.Set;

import javax.persistence.*;

@Entity
public class Paying
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    //cijena i kako se plaća, mjesečno koliko košta koliko košta 
    private String name; 
    private double price;
    private int timeAWeek;
    private String description;

    @OneToMany(mappedBy = "pays")
    private Set<Membership>  memberships;

    //vise clanstva ima isto placanje
    

    /**
     * @return String return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return double return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return int return the timeAWeek
     */
    public int getTimeAWeek() {
        return timeAWeek;
    }

    /**
     * @param timeAWeek the timeAWeek to set
     */
    public void setTimeAWeek(int timeAWeek) {
        this.timeAWeek = timeAWeek;
    }


    /**
     * @return String return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }




    /**
     * @return Integer return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return Set<Membership> return the memberships
     */
    public Set<Membership> getMemberships() {
        return memberships;
    }

    /**
     * @param memberships the memberships to set
     */
    public void setMemberships(Set<Membership> memberships) {
        this.memberships = memberships;
    }

}