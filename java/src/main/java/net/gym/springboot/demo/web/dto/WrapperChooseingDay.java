package net.gym.springboot.demo.web.dto;

import java.util.ArrayList;


public class WrapperChooseingDay {

    private ArrayList<ChoosingDayDto> listIds;
  
    
    /**
     * @return ArrayList<ChoosingDayDto> return the listIds
     */
    public ArrayList<ChoosingDayDto> getListIds() {
        return listIds;
    }

    /**
     * @param listIds the listIds to set
     */
    public void setListIds(ArrayList<ChoosingDayDto> listIds) {
        this.listIds = listIds;
    }

}