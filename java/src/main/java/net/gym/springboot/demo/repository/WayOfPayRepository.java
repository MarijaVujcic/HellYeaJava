package net.gym.springboot.demo.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.gym.springboot.demo.model.WayOfPay;

@Repository
public interface WayOfPayRepository extends JpaRepository<WayOfPay, Integer> {

}