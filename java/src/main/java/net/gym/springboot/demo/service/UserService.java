package net.gym.springboot.demo.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import net.gym.springboot.demo.model.User;
import net.gym.springboot.demo.web.dto.UserRegistrationDto;

@Service
public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);

    void saveU(User u);

}
