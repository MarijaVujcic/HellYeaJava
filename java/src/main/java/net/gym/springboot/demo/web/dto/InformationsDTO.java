package net.gym.springboot.demo.web.dto;




import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;

import org.springframework.lang.NonNull;

public class InformationsDTO
{
  

    private Double height;
    private Double weight;
    @NonNull
    private Integer selectedPay;

    private String iban;
    
    private Date daz;
   
    // private String selectedDays;

    private Integer selectedProgram;

    // @NotNull(message = "You must choose way of paying.")
     private Integer wayPaying;
 
    public InformationsDTO()
    {
        this.daz=Date.valueOf(LocalDate.of(Calendar.getInstance().get(Calendar.YEAR), 1, 1));
    }
    /**
     * @return Double return the height
     */
    public Double getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Double height) {
        this.height = height;
    }

    /**
     * @return Double return the weight
     */
    public Double getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(Double weight) {
        this.weight = weight;
    }



    /**
     * @return Integer return the selectedProgram
     */
    public Integer getSelectedProgram() {
        return selectedProgram;
    }

    /**
     * @param selectedProgram the selectedProgram to set
     */
    public void setSelectedProgram(Integer selectedProgram) {
        this.selectedProgram = selectedProgram;
    }


    /**
     * @return Integer return the selectedPay
     */
    public Integer getSelectedPay() {
        return selectedPay;
    }

    /**
     * @param selectedPay the selectedPay to set
     */
    public void setSelectedPay(Integer selectedPay) {
        this.selectedPay = selectedPay;
    }


  


    /**
     * @return Integer return the wayPaying
     */
    public Integer getWayPaying() {
        return wayPaying;
    }

    /**
     * @param wayPaying the wayPaying to set
     */
    public void setWayPaying(Integer wayPaying) {
        this.wayPaying = wayPaying;
    }


    /**
     * @return String return the iban
     */
    public String getIban() {
        return iban;
    }

    /**
     * @param iban the iban to set
     */
    public void setIban(String iban) {
        this.iban = iban;
    }




    /**
     * @return Date return the daz
     */
    public Date getDaz() {
        return daz;
    }

    /**
     * @param daz the daz to set
     */
    public void setDaz(Date daz) {
        this.daz = daz;
    }

}