package net.gym.springboot.demo.model;

import javax.persistence.Entity;

import java.sql.Date;
import javax.persistence.*;

@Entity
public class DayInMonth
{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    private Date dayD;    

    private Boolean been;
    
    @ManyToOne
    private Month monthS;
    /**
     * @return Integer return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * @return Boolean return the been
     */
    public Boolean isBeen() {
        return been;
    }

    /**
     * @param been the been to set
     */
    public void setBeen(Boolean been) {
        this.been = been;
    }




    /**
     * @return Date return the dayD
     */
    public Date getDayD() {
        return dayD;
    }

    /**
     * @param dayD the dayD to set
     */
    public void setDayD(Date dayD) {
        this.dayD = dayD;
    }


    /**
     * @return Month return the monthS
     */
    public Month getMonthS() {
        return monthS;
    }

    /**
     * @param monthS the monthS to set
     */
    public void setMonthS(Month monthS) {
        this.monthS = monthS;
    }

}