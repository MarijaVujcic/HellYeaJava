package net.gym.springboot.demo.model;


import java.util.Set;

import javax.persistence.*;

@Entity
public class ChoosenDay
{
    private String nameDayWeek;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    
    @OneToMany(mappedBy = "chosen")
    private Set<Selected_days>  chosendays;
    public ChoosenDay()
    {
        
    }
    public ChoosenDay(String s)
    {
        this.nameDayWeek=s;
    }
    /**
     * @return Integer return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }




    /**
     * @return String return the nameDayWeek
     */
    public String getNameDayWeek() {
        return nameDayWeek;
    }

    /**
     * @param nameDayWeek the nameDayWeek to set
     */
    public void setNameDayWeek(String nameDayWeek) {
        this.nameDayWeek = nameDayWeek;
    }


    /**
     * @return Set<Selected_days> return the chosendays
     */
    public Set<Selected_days> getChosendays() {
        return chosendays;
    }

    /**
     * @param chosendays the chosendays to set
     */
    public void setChosendays(Set<Selected_days> chosendays) {
        this.chosendays = chosendays;
    }

}