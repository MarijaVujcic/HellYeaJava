package net.gym.springboot.demo.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Selected_days {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @ManyToOne
    private Membership selected;
    
    @ManyToOne
    private ChoosenDay chosen;
    /**
     * @return Integer return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * @return Membership return the selected
     */
    public Membership getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Membership selected) {
        this.selected = selected;
    }


   


    /**
     * @return ChoosenDay return the chosen
     */
    public ChoosenDay getChosen() {
        return chosen;
    }

    /**
     * @param chosen the chosen to set
     */
    public void setChosen(ChoosenDay chosen) {
        this.chosen = chosen;
    }

}