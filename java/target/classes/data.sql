-- Insert rows into table 'TableName'
INSERT INTO ROLE
( -- columns to insert data into
 id, name
)
VALUES
( -- first row: values for the columns in the list above
 1, ROLE_ADMIN
),
( -- second row: values for the columns in the list above
 2, ROLE_USER
),
(
 3, ROLE_COACH
)
-- add more rows here
GO

